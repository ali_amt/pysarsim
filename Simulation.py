import numpy as np
from SAR import ImageStack

timeEpochs = np.array([0, 6, 13, 16, 20, 25])
Im = ImageStack("sentinel")
Im.setAcquisitionTimes("custom", timeEpochs)

Im.addPoint("A", 1200, 1000, 100)
Im.addPoint("B", 1600, 500, 103.4)
Im.addPoint("C", 1400, 1300, 102.9)
Im.addPoint("D", 1800, 800, 106.3)
Im.addPoint("E", 1700, 1200, 104.6)
Im.addPoint("F", 1500, 900, 103.1)

modes = ["linear", "noise"]

params = { "a": 12, "b": 0, "noise-params": { "dist": "gaussian", "mu": 0, "sigma": 0.01 } }
Im.pointGenerateDeformationTimeseries("A", modes, params)
params["a"] = 13
Im.pointGenerateDeformationTimeseries("B", modes, params)
params["a"] = 11
Im.pointGenerateDeformationTimeseries("C", modes, params)
params["a"] = 12.5
Im.pointGenerateDeformationTimeseries("D", modes, params)
params["a"] = 13.5
Im.pointGenerateDeformationTimeseries("E", modes, params)
params["a"] = 11
Im.pointGenerateDeformationTimeseries("F", modes, params)

Im.generateNetwork("delaunay")
Im.setReferenceTime("mid")
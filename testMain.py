import numpy as np
from SAR import ImageStack

# Define time epochs in (months) ## should be changed to days ## add automatic time epochs generation <start-date, end-date>
# timeEpochs = np.array([0, 6, 13, 16, 20, 25])

# Generate a sentinel image stack in at defined time epochs
# ImageStack(satellite, time-epochs)
# satellite: "sentinel", "radarsat", "terrasar-x" and "tandem-x"
# Im = ImageStack("sentinel")
# Im.setAcquisitionTimes("custom", timeEpochs)

# Add points to the images
# ImageStack.addPoint(id, x, y, h)
# Im.addPoint("A", 1200, 1000, 100)
# Im.addPoint("B", 1600, 500, 103.4)
# Im.addPoint("C", 1400, 1300, 102.9)
# Im.addPoint("D", 1800, 800, 106.3)
# Im.addPoint("E", 1700, 1200, 104.6)
# Im.addPoint("F", 1500, 900, 103.1)

# Generate deformation timeseries for points A B and C
# modes = array of components to be added to deformation : "linear", "seasonal", "noise"
# modes = ["linear", "noise"]
# params = params of deformation components : "a" (cm/year), "b" (cm/year), "seasonal-sin", "seasonal-cos", "noise-params"
# params = { "a": 12, "b": 0, "noise-params": { "dist": "gaussian", "mu": 0, "sigma": 0.01 } }
# ImageStack.pointGenerateDeformationTimeseries(id, modes, params)
# Im.pointGenerateDeformationTimeseries("A", modes, params)
# print(Im.point("A").deformation.timeSeries)

# modes = ["seasonal", "noise"]
# params = { "seasonal-sin": 10, "seasonal-cos": 10, "noise-params": { "dist": "gaussian", "mu": 0, "sigma": 0.01 } }
# Im.pointGenerateDeformationTimeseries("B", modes, params)
# print(Im.point("B").deformation.timeSeries)

# modes = ["linear", "seasonal", "noise"]
# params = { "a": 12, "b": 0, "seasonal-sin": 6, "seasonal-cos": 6, "noise-params": { "dist": "gaussian", "mu": 0, "sigma": 0.01 } }
# Im.pointGenerateDeformationTimeseries("C", modes, params)
# print(Im.point("C").deformation.timeSeries)

# Generate arcs network
# ImageStack.generateNetwork(method, params)
# method = network generation method : "tsp", "spider"
# params = optional parameters for network generation : "hub" ("center" or hub point id), "start" ("auto" or starting point id)

# Example: TSP with start point F   <<<< uncomment below >>>>
# Im.generateNetwork("tsp", {"start": "F"})

## Example: spider with hub point being the central-most point   <<<< uncomment below >>>>
## Im.generateNetwork("spider", {"hub": "center"})

### Example: spider with hub point D    <<<< uncomment below >>>>
### Im.generateNetwork("spider", {"hub": "D"})

#### Example: custom arcs     <<<< uncomment below >>>>
#### Im.createArcByID("A","B")
#### Im.createArcByID("B","F")
#### Im.createArcByID("D","E")
#### Im.createArcByID("E","F")
#### Im.createArcByID("C","F")

##### add full graph (full connection)
##### Im.generateNetwork("full")

###### Example: delaunay network <<<< uncomment below >>>>
###### Im.generateNetwork("delaunay")


# Plot deformation of point with id
# ImageStack.pointPlotDeformation(id)
# Im.pointPlotDeformation("B")

# Plot points and arcs of the image stack
# ImageStack.plot(mode)
# mode = what to plot : "points", "arcs"
# Im.plotNetwork("arcs")


###### Test
# print(Im.point("A").deformation.timeSeries)
#
# params = { "a": 13, "b": 0, "noise-params": { "dist": "gaussian", "mu": 0, "sigma": 0.01 } }
# Im.pointGenerateDeformationTimeseries("B", modes, params)
# print(Im.point("B").deformation.timeSeries)
#
# Im.pointPlotDeformation("A")
#
# Im.pointTimeDifference("all", { "ref": "mid"})
# print(Im.point("A").deformation.timeEpochs)
# print(Im.point("A").deformation.timeSeries)
# print(Im.point("B").deformation.timeSeries)
#
# Im.pointPlotDeformation("A")
#
# Im.createArcByID("A", "B")
# print(Im.arc("A", "B").deformation.timeEpochs)
# print(Im.arc("A", "B").deformation.timeSeries)
#
# Im.arcPlotDeformation("A", "B")

# print(np.argwhere(Im.points == Im.point("B")))



##### Scenario

timeEpochs = np.array([0, 7, 13, 16, 20, 25, 31, 36, 42])
Im = ImageStack("sentinel")
Im.setAcquisitionTimes("custom", timeEpochs)

Im.addPoint("A", 1200, 1000, 100)
Im.addPoint("B", 1600, 500, 103.4)
Im.addPoint("C", 1400, 1300, 102.9)
Im.addPoint("D", 1800, 800, 106.3)
Im.addPoint("E", 1700, 1200, 104.6)
Im.addPoint("F", 1500, 900, 103.1)

modes = ["linear", "noise"]

params = { "a": 0.12, "b": 0, "noise-params": { "dist": "gaussian", "mu": 0, "sigma": 0.0001 } }
Im.pointGenerateDeformationTimeseries("A", modes, params)
params["a"] = 0.13
Im.pointGenerateDeformationTimeseries("B", modes, params)
params["a"] = 0.11
Im.pointGenerateDeformationTimeseries("C", modes, params)
params["a"] = 0.125
Im.pointGenerateDeformationTimeseries("D", modes, params)
params["a"] = 0.135
Im.pointGenerateDeformationTimeseries("E", modes, params)
params["a"] = 0.11
Im.pointGenerateDeformationTimeseries("F", modes, params)


#print(Im.point("A").deformation.timeSeries)
Im.pointsDeformationToPhase()
#print(Im.point("A").deformation.timeSeries)
Im.pointsWrapPhase()
#print(Im.point("A").deformation.timeSeries)

Im.generateNetwork("delaunay")
Im.setReferenceTime("mid")
Im.satellite.setReferenceTime(2)

# print(Im.arc("A","B").deformation.timeSeries)
# Im.doTimeDifference()
res = Im.generateSatelliteInputsTest()
# print(res)
A = Im.generateDesignMatrix("A", res["A"], "B", res["B"])
print(A)
y = Im.generateObservations("A", res["A"], "B", res["B"])
print(y)
print(np.linalg.inv(np.transpose(A).dot(A)).dot(np.transpose(A)).dot(y))


# Im.arc("A","B").setReferenceTime(2)
# Im.arc("A","B").deformationTimeDifference()
# print(Im.arc("A","B").deformation.timeSeries)


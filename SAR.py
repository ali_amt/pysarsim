import numpy as np
import matplotlib.pyplot as pplt
import scipy.spatial as ss
import ast
import math


def checkIfInArray(arr, element):
    for i in range(arr.size):
        if element is arr[i]:
            return True

    return False

sin = lambda x: math.sin(math.pi*x/180)
cos = lambda x: math.cos(math.pi*x/180)
tand = lambda x: math.tan(math.pi*x/180)
tan = lambda x: math.tan(x)
atand = lambda x: math.atan(x)*math.pi/180
atan = lambda x: math.atan(x)

class Deformation:
    def __init__(self, height=None, time=None):
        self.height = height
        self.timeEpochs = time
        self.timeSeries = None
        self.deformationModes = np.array([])
        self.referenceTime = None
        if time is not None and height is not None:
            self.timeSeries = np.ones(time.shape)*height

    def addLinearComponent(self, a, b):
        time = self.timeEpochs
        if self.timeSeries is None:
            self.timeSeries = time*a/12 + b
        else:
            self.timeSeries = self.timeSeries + time*a/12 + b
        self.deformationModes = np.append(self.deformationModes, "linear")
        #self.deformationModes = np.append(self.deformationModes, {"mode": "linear", "a": a, "b": b})

    def addSeasonalComponent(self, a, b):
        time = self.timeEpochs
        A = np.sin(time*2*np.pi/12)
        B = np.cos(time*2*np.pi/12)
        if self.timeSeries is None:
            self.timeSeries = A*a/12 + B*b/12
        else:
            self.timeSeries = self.timeSeries + A*a/12 + B*b/12
        self.deformationModes = np.append(self.deformationModes, "seasonal")
        #self.deformationModes = np.append(self.deformationModes, {"mode": "seasonal", "sin": a, "cos": b})

    def generateTimeseries(self, modes, params):
        if "linear" in modes:
            self.addLinearComponent(params["a"], params["b"])

        if "seasonal" in modes:
            self.addSeasonalComponent(params["seasonal-sin"], params["seasonal-cos"])

        if "noise" in modes:
            self.generateNoise(params["noise-params"])

    def addGaussianNoise(self, mu, sigma):
        time = self.timeEpochs
        if self.timeSeries is None:
            self.timeSeries = np.random.normal(mu, sigma, time.shape)
        else:
            self.timeSeries = self.timeSeries + np.random.normal(mu, sigma, time.shape)
        self.deformationModes = np.append(self.deformationModes, "gaussian")
        #self.deformationModes = np.append(self.deformationModes, {"mode": "gaussian", "mu": mu, "sigma": sigma})

    def generateNoise(self, params):
        if params["dist"] == "gaussian":
            self.addGaussianNoise(params["mu"], params["sigma"])

    def toPhase(self, waveLength):
        self.timeSeries = self.timeSeries*-4*np.pi/waveLength

    def plotTimeseries(self):
        pplt.plot(self.timeEpochs, self.timeSeries, linestyle="--", marker="o", color="b")
        pplt.xlabel("time epochs (month)")
        pplt.ylabel("deformation")
        pplt.show()

    def timeDifference(self):
        n = self.timeSeries.size
        ref = self.timeSeries[np.argwhere(self.timeEpochs == self.referenceTime)[0]]
        tmp = np.array([])
        for element in self.timeSeries:
            tmp = np.append(tmp, [ref-element])
        self.timeSeries = tmp
        self.timeSeries = np.delete(self.timeSeries, np.argwhere(self.timeSeries == 0))
        self.timeEpochs = np.delete(self.timeEpochs, np.argwhere(self.timeEpochs == self.referenceTime))

    def setReferenceTime(self, ref):
        n = self.timeSeries.size
        if ref == "start":
            ep = self.timeEpochs[0]
        elif ref == "mid":
            ep = self.timeEpochs[int(n / 2 - 1)] if n % 2 == 0 else self.timeEpochs[math.floor(n / 2)]
        elif ref == "end":
            ep = self.timeEpochs[n - 1]
        else:
            ep = self.timeEpochs[ref]

        self.referenceTime = ep
        return ep

    def wrapPhase(self):
        self.timeSeries = np.mod(self.timeSeries+np.pi, 2*math.pi) - math.pi


class Point:
    def __init__(self, x, y, h, id, time=None):
        self.id = id
        self.x = x
        self.y = y
        self.coordinates2D = np.array([x, y])
        self.height = h
        self.coordinates3D = np.array([x, y, h])
        self.deformation = Deformation(h, time)

    def deformationToPhase(self, waveLength):
        self.deformation.toPhase(waveLength)

    def wrapDeformationPhase(self):
        self.deformation.wrapPhase()

    def plotDeformation(self):
        pplt.title("Point " + self.id + " deformation timeseries")
        self.deformation.plotTimeseries()

    def deformationTimeDifference(self):
        self.deformation.timeDifference()

    def setReferenceTime(self, ref):
        return self.deformation.setReferenceTime(ref)

class Arc:
    def __init__(self, point1, point2):
        self.point1 = point1
        self.point2 = point2
        self.pID1 = point1.id
        self.pID2 = point2.id
        self.deformation = Deformation(None, point1.deformation.timeEpochs)
        self.deformation.timeSeries = point1.deformation.timeSeries - point2.deformation.timeSeries

    def replacePoint(self, oldPoint, newPoint):
        if oldPoint is self.point1:
            self.point1 = newPoint
        elif oldPoint is self.point2:
            self.point2 = newPoint

        self.deformation.timeSeries = self.point1.deformation.timeSeries - self.point2.deformation.timeSeries

    def deformationToPhase(self, waveLength):
        self.deformation.toPhase(waveLength)

    def wrapDeformationPhase(self):
        self.deformation.wrapPhase()

    def deformationTimeDifference(self):
        self.deformation.timeDifference()

    def setReferenceTime(self, ref):
        return self.deformation.setReferenceTime(ref)

    def plotDeformation(self):
        pplt.title("Arc (" + self.pID1 + ","+ self.pID2 + ") deformation timeseries")
        self.deformation.plotTimeseries()


class ImageStack:
    def __init__(self, satellite="sentinel"):
        f = open("settings.txt", "r")
        data = f.read()
        f.close()
        data = ast.literal_eval(data)
        data = data[satellite]
        self.waveLength = data["wavelength"]
        self.repeatOrbit = data["repeat-orbit"]
        self.baseline = data["perp-baseline"]
        self.points = np.array([])
        self.arcs = np.array([])
        self.satellite = SatelliteStack(data)
        self.pointsIDMap = np.array([])
        self.timeEpochs = None
        self.referenceTime = None

    def addPointsFromFile(self, path):
        f = open(path, "r")
        for line in f:
            p = line.split(',')
            if len(p) < 5:
                self.addPoint(p[0], float(p[1]), float(p[2]), float(p[3]))
            else:
                self.addPoint(p[0], float(p[1]), float(p[2]), float(p[3]))
                modes = p[4].split('&')
                # to be continued

    def addPoint(self, id, x, y, h):
        point = Point(x, y, h, id, self.timeEpochs)
        self.points = np.append(self.points, [point])
        self.pointsIDMap = np.append(self.pointsIDMap, id)

    def createArc(self, point1, point2):
        arc = Arc(point1, point2)
        self.arcs = np.append(self.arcs, [arc])

    def createArcByID(self, id1, id2):
        for p in self.points:
            if p.id == id1:
                point1 = p
            elif p.id == id2:
                point2 = p

        arc = Arc(point1, point2)
        self.arcs = np.append(self.arcs, [arc])

    def point(self, id):
        for p in self.points:
            if p.id == id: return p

    def arc(self, id1, id2):
        for a in self.arcs:
            if a.pID1 == id1 and a.pID2 == id2: return a
            # if a.pID1 == id2 and a.pID2 == id1: return a

    def time(self, ind):
        return self.timeEpochs[ind]

    def listPoints(self, columns):
        m = len(columns)
        list = np.empty((0, m), float)
        for p in self.points:
            row = np.array([])
            if "id" in columns:
                row = np.append(row, [p.id])
            if "x" in columns:
                row = np.append(row, [p.x])
            if "y" in columns:
                row = np.append(row, [p.y])
            if "h" in columns:
                row = np.append(row, [p.height])

            list = np.vstack([list, row])

        return list

    def setTimesAutomatically(self, number):
        self.timeEpochs = np.divide(list(range(0, number * self.repeatOrbit, self.repeatOrbit)), 30)

    def setTimesCustom(self, number):
        self.timeEpochs = np.divide(list(number), 30)

    def setAcquisitionTimes(self, mode, params):
        if mode == "auto":
            self.setTimesAutomatically(params["images-number"])
        elif mode == "custom":
            self.setTimesCustom(params)

        #self.satellite.timeEpochs = self.timeEpochs
        self.satellite.setAcquisitionTimes(mode, params)

    def pointGenerateDeformationTimeseries(self, id, mode, params):
        for p in self.points:
            if p.id == id:
                p.deformation.generateTimeseries(mode, params)

    def pointsDeformationToPhase(self):
        for point in self.points:
            point.deformationToPhase(self.waveLength)

    def arcsDeformationToPhase(self):
        for arc in self.arcs:
            arc.deformationToPhase(self.waveLength)

    def pointsWrapPhase(self):
        for point in self.points:
            point.wrapDeformationPhase()

    def arcsWrapPhase(self):
        for arc in self.arcs:
            arc.wrapDeformationPhase()

    def getCentroid(self, dim=2):
        if dim == 2:
            sum = np.array([0, 0])
            for p in self.points:
                sum = sum + p.coordinates2D
        elif dim == 3:
            sum = np.array([0, 0, 0])
            for p in self.points:
                sum = sum + p.coordinates3D

        return sum/self.points.size

    def doSpider(self, hub):
        if hub == "center":
            centroid = self.getCentroid()
            minDist = math.dist(centroid, self.points[0].coordinates2D)
            minPoint = self.points[0]
            for p in self.points:
                dist = math.dist(centroid, p.coordinates2D)
                if dist < minDist:
                    minPoint = p

        else:
            minPoint = self.point(hub)

        for p in self.points:
            if p is minPoint: continue
            self.createArc(minPoint, p)

    def doTSP(self, start):
        minDist = 100000000
        n = self.points.size
        if start == "auto":
            for i in range(n-1):
                for j in range(i+1, n):
                    p1 = self.points[i]
                    p2 = self.points[j]
                    dist = math.dist(p1.coordinates2D, p2.coordinates2D)
                    if dist < minDist:
                        minDist = dist
                        visited = np.array([p1, p2])
                        start = p2
        else:
            start = self.point(start)
            visited = np.array([start])

        while(visited.size != self.points.size):
            p1 = start
            minDist = 10000000
            for p2 in self.points:
                if p2 is not start and not checkIfInArray(visited, p2):
                    dist = math.dist(p1.coordinates2D, p2.coordinates2D)
                    if dist < minDist:
                        start = p2
                        minDist = dist
            visited = np.append(visited, [start])

        for i in range(visited.size):
            p1 = visited[i]
            if i == visited.size-1:
                p2 = visited[0]
            else:
                p2 = visited[i+1]
            self.createArc(p1, p2)

    def doDelaunay(self):
        pts =  self.listPoints(["x", "y"])
        delTri = np.array(ss.Delaunay(pts).simplices.copy())
        delTri = np.vstack([delTri[:,0:2], delTri[:,1:3], np.hstack([delTri[:,0:1], delTri[:,2:3]])])
        n = self.points.size
        adjMatrix = np.zeros((n, n))

        for i in range(delTri.shape[0]):
            adjMatrix[delTri[i,0], delTri[i,1]] = 1
            adjMatrix[delTri[i,1], delTri[i,0]] = 1

        for i in range(n-1):
            for j in range(i+1, n):
                if adjMatrix[i, j] == 1:
                    self.createArcByID(self.pointsIDMap[i], self.pointsIDMap[j])

    def doFullGraph(self):
        n = self.points.size
        for i in range(n-1):
            for j in range(i+1, n):
                self.createArcByID(self.pointsIDMap[i], self.pointsIDMap[j])

    def generateNetwork(self, method, params=None):
        self.arcs = np.array([])
        if method == "spider":
            self.doSpider(params["hub"])
        elif method == "tsp":
            self.doTSP(params["start"])
        elif method == "delaunay":
            self.doDelaunay()
        elif method == "full":
            self.doFullGraph()

    def setReferenceTime(self, ref):
        n = self.timeEpochs.size
        if ref == "start":
            ep = self.timeEpochs[0]
        elif ref == "mid":
            ep = self.timeEpochs[int(n / 2 - 1)] if n % 2 == 0 else self.timeEpochs[math.floor(n / 2)]
        elif ref == "end":
            ep = self.timeEpochs[n - 1]
        else:
            ep = self.timeEpochs[ref]

        self.referenceTime = ep

    # def pointTimeDifference(self, pt="all"):
    #     if pt == "all":
    #         for point in self.points:
    #             point.deformationTimeDifference()
    #     else:
    #         self.point(pt).deformationTimeDifference()
    #
    #     self.timeEpochs = np.delete(self.timeEpochs, np.argwhere(self.timeEpochs == self.referenceTime))
    #
    # def arcTimeDifference(self, arc="all"):
    #     if arc == "all":
    #         for a in self.arcs:
    #             a.deformationTimeDifference()
    #     else:
    #         self.arc(arc).deformationTimeDifference()
    #
    #     self.timeEpochs = np.delete(self.timeEpochs, np.argwhere(self.timeEpochs == self.referenceTime))

    def doTimeDifference(self, mode="arc", which="all"):
        ref = np.argwhere(self.timeEpochs == self.referenceTime)[0]
        if which == "all":
            if mode == "point":
                for a in self.points:
                    a.setReferenceTime(ref)
                    a.deformationTimeDifference()
            elif mode == "arc":
                for a in self.arcs:
                    a.setReferenceTime(ref)
                    a.deformationTimeDifference()
        else:
            if mode == "point":
                self.point(which).setReferenceTime(ref)
                self.point(which).deformationTimeDifference()
            elif mode == "arc":
                self.arc(which[0], which[1]).setReferenceTime(ref)
                self.arc(which[0], which[1]).deformationTimeDifference()
                print("Shit")

        #self.timeEpochs = np.delete(self.timeEpochs, ref)
        self.satellite.setReferenceTime(ref)
        # self.satellite.setAcquisitionTimes("custom", np.multiply(self.timeEpochs, 30))

    def pointPlotDeformation(self, id):
        self.point(id).plotDeformation()

    def arcPlotDeformation(self, id1, id2):
        self.arc(id1, id2).plotDeformation()

    def plotNetwork(self, mode="points"):
        pplt.axis("equal")
        if mode == "arcs":
            for a in self.arcs:
                pplt.plot([a.point1.x, a.point2.x], [a.point1.y, a.point2.y], "b-")

        for p in self.points:
            pplt.plot(p.x, p.y, "ro")
            pplt.text(p.x, p.y, p.id)

        pplt.title("Image Stack Network")
        pplt.xlabel("X coordinate")
        pplt.ylabel("Y coordinate")
        pplt.show()

    def generateSatelliteInputsTest(self, params={"baseline": {"mode": "random"}, "altitude": {"mode": "random"}, "inc-angle": {"mode": "uniform"} }):
        self.satellite.generatePerpBaselines(params["baseline"]["mode"])
        coordinates = self.satellite.calculateSatellitesPosition("centroid", self.getCentroid())
        return self.satellite.generateInputsTestMode(coordinates, self.points, "2D")

    def generateDesignMatrix(self, p1ID, p1Inputs, p2ID, p2Inputs):
        n = p1Inputs.size

        m1 = 2 ## m1 = 1
        modes1 = self.point(p1ID).deformation.deformationModes
        if "linear" in modes1:
            m1 += 1
        if "seasonal" in modes1:
            m1 += 2

        m2 = 2  ## m2 = 1
        modes2 = self.point(p2ID).deformation.deformationModes
        if "linear" in modes2:
            m2 += 1
        if "seasonal" in modes2:
            m2 += 2

        m = np.maximum(m1, m2)
        design = np.zeros((n, m))
        baselines = self.satellite.perpBaselines
        times = self.timeEpochs
        l = self.waveLength/100

        for i in range(n):
            inp1 = p1Inputs[i]
            inp2 = p2Inputs[i]
            b = baselines[i]
            t = times[i]
            row = np.array([-4 * math.pi * b / (l * inp1["r"] * sin(inp1["theta"])) , 4 * math.pi * b / (l * inp2["r"] * sin(inp2["theta"]))])
            #row = np.array([-4 * math.pi * b / (l * inp1["r"] * sin(inp1["theta"]))]) - np.array([-4 * math.pi * b / (l * inp2["r"] * sin(inp2["theta"]))])
            # row = np.array([-4 * math.pi * b / (l * ((inp1["r"]+inp2["r"])/2) * sin((inp1["theta"]+inp2["theta"])/2))])
            if "linear" in modes1 or "linear" in modes2:
                row = np.append(row, [4 * math.pi * t / l])
            # elif "linear" in modes2:
            #     row = np.append(row, [-4 * math.pi * t / l])

            if "seasonal" in modes1 or "seasonal" in modes2:
                row = np.append(row, [4 * math.pi * sin(2*math.pi*t/12) / l, 4 * math.pi * cos(2*math.pi*t/12) / l])
            # elif "seasonal" in modes2:
            #     row = np.append(row, [-4 * math.pi * sin(2 * math.pi * t / 12) / l, -4 * math.pi * cos(2 * math.pi * t / 12) / l])

            design[i,:] = row

        return design

    def generateObservations(self, p1ID, p1Inputs, p2ID, p2Inputs):
        n = p1Inputs.size
        p1 = self.point(p1ID)
        p2 = self.point(p2ID)
        h1 = p1.height
        h2 = p2.height
        d1 = p1.deformation.timeSeries - h1
        d2 = p2.deformation.timeSeries - h2
        baselines = self.satellite.perpBaselines
        times = self.timeEpochs
        l = self.waveLength/100

        obs = np.array([])
        for i in range(n):
            b = baselines[i]
            t = times[i]
            r1 = p1Inputs[i]["r"]
            theta1 = p1Inputs[i]["theta"]
            r2 = p2Inputs[i]["r"]
            theta2 = p2Inputs[i]["theta"]
            row = -4*np.pi*b*(h1/(r1*sin(theta1)) - h2/(r2*sin(theta2)))/(l) + 4*np.pi*(d1[i]-d2[i])/(l)
            # obs = np.append(obs, row)
            obs = np.append(obs, np.mod(row+np.pi, 2*np.pi)-np.pi)

        return obs


class SatelliteStack:
    def __init__(self, params, times=None):
        self.waveLength = params["wavelength"]
        self.repeatOrbit = params["repeat-orbit"]
        self.incidentAngle = params["incident-angle"]
        self.resolution = params["resolution"]
        self.altitude = params["altitude"]*1000
        self.baseline = params["perp-baseline"]
        self.perpBaselines = None
        self.timeEpochs = times
        self.referenceTime = None

    def setTimesAutomatically(self, number):
        self.timeEpochs = np.divide(list(range(0, number*self.repeatOrbit, self.repeatOrbit)), 30)

    def setTimesCustom(self, number):
        self.timeEpochs = np.divide(list(number), 30)

    def setAcquisitionTimes(self, mode, params):
        if mode == "auto":
            self.setTimesAutomatically(params["images-number"])
        elif mode == "custom":
            self.setTimesCustom(params)

    def setReferenceTime(self, ind):
        self.referenceTime = self.timeEpochs[ind]

    def generatePerpBaselines(self, mode, params=None):
        if mode == "random":
            if params is None:
                self.perpBaselines = np.array(np.random.normal(self.baseline["mu"], self.baseline["sigma"], self.timeEpochs.shape))
                self.perpBaselines[np.argwhere(self.timeEpochs == self.referenceTime)[0]] = 0
            # other params should be implemented ...
        # other modes should be implemented ...

    # def generateInputsTest(self, params):
    #     bl = params["baseline"]
    #     self.generatePerpBaselines(bl["mode"])

    def calculateSatellitesPosition(self, mode, params):
        inc = self.incidentAngle
        alt = self.altitude
        n = self.perpBaselines.size
        coordinates = np.zeros((n, 3))
        if mode == "centroid":
            centroid = params
            for i in range(n):
                bl = self.perpBaselines[i]
                coordinates[i, :] = [ centroid[0]+alt*tand(inc)+bl*cos(inc), centroid[1], alt-bl*sin(inc) ]

        return coordinates

    def generateInputsTestMode(self, satcoords, points, mode):
        n = len(satcoords)
        m = len(points)
        inputs = {}
        if mode == "2D":
            for point in points:
                id = point.id
                pcrd = np.array([point.x, point.y, point.height])
                element = np.array([])
                for scrd in satcoords:
                    dif = np.subtract(scrd, pcrd)
                    element = np.append(element, {"theta": atan(dif[0]/dif[2])*180/math.pi, "r": np.linalg.norm(np.array([dif[0], dif[2]]))})

                inputs[id] = element
        elif mode == "3D":
            for point in points:
                id = point.id
                pcrd = np.array([point.x, point.y, point.height])
                element = np.array([])
                for scrd in satcoords:
                    dif = np.subtract(scrd, pcrd)
                    r = np.linalg.norm(dif[0:2])
                    element = np.append(element, {"theta": atan(r/dif[2])*180/math.pi, "r": np.linalg.norm(dif)})

                inputs[id] = element

        return inputs
